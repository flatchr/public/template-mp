# FLATCHR TEMPLATE MP

## Context

### [Flatchr](https://flatchr.io/) 
Flatchr has several webhooks that can be configured for free to communicate with third-party applications when certain events are triggered. Flatchr also allows your customer to enter information (token, company id) so that you can identify requests. More information on existing integrations: https://help.flatchr.io/fr/utiliser-la-marketplace

This sample app will allow you to set up a micro-service to simulate retrieving data from Flatchr, build your integration, and send things back to Flatchr as needed. This information will be available in your flatchr's interface on the candidate with your logo.

## Template purpose
Send datas to your application

curl --location --request POST 'localhost:3000/app-exemple/exemple' \
--header 'Content-Type: application/json' \
--data-raw '{
    "token": "tokenprovidedbyyou",
    "firstName": "John",
    "lastName": "Doe",
    "email": "john@flatchr.io",
    "locale": "fr_FR"
}'

Handle results and add additionnal datas to flatchr

curl --location --request POST 'localhost:3000/app-exemple/exemple-hook' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "john@flatchr.io",
    "companyId": "42"
}'

## Documentation 

- [Flatchr API](https://developers.flatchr.io/)
- [Webhooks](https://developers.flatchr.io/webhooks)
